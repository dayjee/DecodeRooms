﻿using UnityEngine;
using System.Collections;

public class Seven_Touch : MonoBehaviour
{

    protected bool letPlay = true;
    public GameObject controlObject;
    public ParticleSystem ps;

    // Use this for initialization
    void Start()
    {
        ps = GetComponent<ParticleSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                Debug.Log("Hit" + hitInfo.transform.gameObject.name);

                if (hitInfo.transform.gameObject.tag == "Seven")
                {
                    Debug.Log("It's working!");

                    letPlay = !letPlay;

                }
                else
                {
                    Debug.Log("nopz");


                }
            }

            else
            {
                Debug.Log("No hit");
            }
        }

        if (letPlay)
        {
            if (!ps.isPlaying)
            {
                ps.Play();
            }
        }
        else
        {
            if (ps.isPlaying)
            {
                ps.Stop();
            }
        }
    }
}

