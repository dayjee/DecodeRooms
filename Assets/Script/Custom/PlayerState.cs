﻿public static class PlayerState
{
	public enum Type
	{
		None,
		Moving,
		Reading,
		Zoomin,
		ZoomOut,
		UsingItem,
		ShowingItem
	}
}
