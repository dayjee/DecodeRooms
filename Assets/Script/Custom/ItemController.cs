﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemController : MonoBehaviour {

	public List<string> InventoryItems;

	[SerializeField]
	private GameObject inventoryPopup;

	[SerializeField]
	List<GameObject> itemSlots;

	private int previousIndex = 0;
	private int index = 0;
	private static int MaxCountOfItemSlot = 8;
	private static int MinCountOfItemSlot = 0;


	void Start()
	{
		Init();

		foreach (Transform itemSlot in GetChildren(inventoryPopup.transform))
		{
			itemSlots.Add(itemSlot.gameObject);
		}
	}

	void LateUpdate()
	{
		#if UNITY_EDITOR
		ItemControll();

		if (inventoryPopup.activeSelf)
		{
			SelectItem();
		}
		#endif
	}

	private void Init()
	{
		InventoryItems = new List<string>();
	}

	public void AddItemToInventory(string itemName)
	{
		InventoryItems.Add(itemName);
	}

	public void ShowInventoryItems()
	{
		index = 0;

		if (inventoryPopup.activeSelf)
		{
			inventoryPopup.SetActive(false);
			return;
		}
		else
		{
			inventoryPopup.SetActive(true);
			foreach (string itemName in InventoryItems)
			{
				Debug.Log(itemName);
			}
		}
		//TODO : Make popup with show get items.
	}

	public bool UseItem(GameObject selectedItem)
	{
		//TODO : return true when selecteditem can use.
		return false;
	}
		
	private void ItemControll()
	{
		if (Input.GetKeyUp(KeyCode.Semicolon))
		{
			ShowInventoryItems();
		}
	}

	private void ChangeActivateItemSlot()
	{
		itemSlots[index].transform.GetChild(0).gameObject.SetActive(true);
		itemSlots[previousIndex].transform.GetChild(0).gameObject.SetActive(false);
		previousIndex = index;
	}

	private void SelectItem()
	{
		previousIndex = index;

		if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			Debug.Log(itemSlots.Count);
			if (index + 1 < MaxCountOfItemSlot)
			{
				index++;
				ChangeActivateItemSlot();
			}
		}
		else if (Input.GetKeyUp(KeyCode.LeftArrow))
		{
			if (index != MinCountOfItemSlot)
			{
				index--;
				ChangeActivateItemSlot();
			}
		}
		else if (Input.GetKeyUp(KeyCode.DownArrow))
		{
			UseItem(itemSlots[index]);
			inventoryPopup.SetActive(false);
		}
	}

	private List<Transform> GetChildren(Transform transform)
	{
		List<Transform> transformList = new List<Transform>();

		for (int i = 0; i < inventoryPopup.transform.childCount; i++)
		{
			transformList.Add(inventoryPopup.transform.GetChild(i));
		}

		return transformList;	
	}
}
