﻿using UnityEngine;

public class DataManager : Singleton<DataManager>
{
	public DataManager(){}

	public string CurrentTarget = string.Empty;

	public PlayerState.Type CurrentState = PlayerState.Type.None;
}