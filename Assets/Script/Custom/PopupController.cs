﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class PopupController : MonoBehaviour {

	[SerializeField]
	private Text[] textObjects;

	void Update()
	{
		if (Input.GetKeyUp(KeyCode.Space))
		{
			gameObject.SetActive(false);
		}
	}

	public void SetText(string informationText)
	{
		foreach (Text textObject in textObjects)
		{
			textObject.text = informationText;
		}
	}
		
}
