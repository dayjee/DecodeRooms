﻿public static class EventItemName
{
	public static string Vase = "vase";
	public static string Candle = "candle";
	public static string SmallDeskKey = "smallDesk_key";
	public static string TreasureBox = "treasureBox_lock";
	public static string TreasureBoxKey = "treasureBox_key";
	public static string KeyPadLock = "padlock";
}