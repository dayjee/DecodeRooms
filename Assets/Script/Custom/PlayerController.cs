﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	[SerializeField]
	private Transform MainCamera;

	private ItemController itemController;

	private string currentSeeing = string.Empty;
	private GameObject CurrentSeeObject = null;

	private static float TouchableDistance = 15;

	public void SetCurrentSeeObject(GameObject seeObject)
	{
		CurrentSeeObject = seeObject;
	}

	private float GetDistanceOfTwoObject()
	{
		return Vector3.Distance(CurrentSeeObject.transform.position, gameObject.transform.position);
	}

	public void TouchObject()
	{
		if (GetDistanceOfTwoObject() < TouchableDistance)
		{
			if (CurrentSeeObject)
			{
				Animation animation = CurrentSeeObject.GetComponent<Animation>();
				if (animation)
				{
					animation.Play();
				}
			}
		}
	}

	void Start ()
	{
		DataManager.Instance.CurrentState = PlayerState.Type.None;
		itemController = gameObject.GetComponent<ItemController>();
	}
	
	void Update ()
	{
		#if UNITY_EDITOR
		if (DataManager.Instance.CurrentState == PlayerState.Type.None)
		{
			Moving();
		}

		if (Input.GetKeyUp(KeyCode.Z))
		{
			if (DataManager.Instance.CurrentState == PlayerState.Type.Zoomin)
			{
				MainCamera.localPosition =  new Vector3(0, 0, 0);
				DataManager.Instance.CurrentState = PlayerState.Type.None;
			}
			else
			{
				MainCamera.localPosition = new Vector3(0, 0, 10);
				DataManager.Instance.CurrentState = PlayerState.Type.Zoomin;
			}
		}
		#endif
	}

	void OnCollisionEnter(Collision collision)
	{
		DataManager.Instance.CurrentTarget = GetComponent<Collider>().gameObject.tag;
		Debug.Log("Current TargetName : " + DataManager.Instance.CurrentTarget);
		
		if (collision.gameObject.tag == "Candle")
		{
			itemController.AddItemToInventory(collision.gameObject.tag);
			DestroyObject(collision.gameObject);
		}
	}

	void OnCollisionExit()
	{
		DataManager.Instance.CurrentTarget = string.Empty;
	}

	private void Moving()
	{
		if (Input.GetKey(KeyCode.A))
		{
			transform.Translate(Vector3.left * Time.deltaTime * 20);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.Translate(Vector3.right * Time.deltaTime * 20);
		}

		if (Input.GetKey(KeyCode.W))
		{
			transform.Translate(Vector3.forward * Time.deltaTime * 20);
		}

		if (Input.GetKey(KeyCode.S))
		{
			
			transform.Translate(Vector3.back * Time.deltaTime * 20);
		}

		transform.localPosition = new Vector3(transform.localPosition.x, 21, transform.localPosition.z);
	}

	private void Rotating()
	{
		if (Input.GetKey(KeyCode.E))
		{
			this.transform.Rotate(0, 90.0f * Time.deltaTime, 0);
		}

		else if (Input.GetKey(KeyCode.Q))
		{
			this.transform.Rotate(0, -90.0f * Time.deltaTime, 0);
		}
	}
}
