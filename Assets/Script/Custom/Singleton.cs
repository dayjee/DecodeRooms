using UnityEngine;

public abstract class Singleton<ClassType> where ClassType : class
{
    protected Singleton()
    {
    }

    private static ClassType instance;

    public static ClassType Instance
    {
        get { if (instance == null) Create(); return instance; }
    }

    public static void Create(params object[] val)
    {
        if (val.Length == 1 && val[0].GetType() == typeof(ClassType))
            instance = val[0] as ClassType;
        else
            instance = System.Activator.CreateInstance(typeof(ClassType), val) as ClassType;
    }

    public static void Create()
    {
        instance = System.Activator.CreateInstance(typeof(ClassType)) as ClassType;
    }

    public static void Destroy()
    {
        instance = null;
    }
}
